import numpy as np
import os
import AppLib as ssm
# spark tools
# from pyspark.mllib.clustering import LDA, LDAModel
# import pyspark.mllib.linalg.distributed as pymld

from gensim.models.ldamodel import LdaModel

class Analysis():
    '''
    '''
    def __init__(self):
        self.ldaModelBoolean = False
        self.ldaModel = None
    
    def getDocumentTopic(self,corpus, dictionary):
        '''
        '''
        output = []
        
        if not self.ldaModelBoolean :
            self.ldaModel = LdaModel(corpus=corpus, num_topics=self.nbTopic) #id2word=dictionary, #, update_every=1, chunksize=10000, passes=1)
            self.ldaModelBoolean = True
        else :
            self.ldaModel.update(corpus)
            
        documentTopic = np.zeros((len(corpus),self.nbTopic))
            
        for i,bow in enumerate(corpus) :
            documentTopic[i,:] = map(lambda x:x[1],self.ldaModel.get_document_topics(bow, minimum_probability=0))
        
        output.append(np.transpose(documentTopic))
        output.append(documentTopic)
        
        return output