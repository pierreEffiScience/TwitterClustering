import AppLib as ssm
# streaming tools
import tweepy
import json
import nltk
import sys
import pymongo
from multiprocessing import Process
from threading import Thread
from math import sqrt

from flask import Flask, render_template, session, request
from flask.ext.socketio import SocketIO, emit, join_room, leave_room, \
    close_room, disconnect
# from pyspark.mllib.linalg import Vectors
# from pyspark.mllib.clustering import KMeans, KMeansModel
from sklearn.cluster import KMeans
import numpy as np

def startListening(auth, api, socketio, sc, keyword = 'EffiScience') :
    '''
    '''
    listener = ssm.GetStreamListener(api, socketio, sc, keyword)
    streamingApi = tweepy.streaming.Stream(auth, listener)
    # SF  =-122.75,36.8,-121.75,37.8
    # Paris = 2.08,48.65,2.63,49.04
    # London = -0.56,51.26,0.28,51.68
    # UK = [-6.38,49.87,1.77,55.81]
    # Bounding boxes for geolocations
    # Online-Tool to create boxes (c+p as raw CSV): http://boundingbox.klokantech.com/
    # GEOBOX_WORLD = [-180,-90,180,90]
    # GEOBOX_GERMANY = [5.0770049095, 47.2982950435, 15.0403900146, 54.9039819757]
    # streamingApi.filter(locations=GEOBOX_WORLD, async = True)
    streamingApi.filter(track=[keyword], async = True)
    
    output = streamingApi
    return output
	
def stopListening(streamingApi):
	'''
	'''
	streamingApi.disconnect()
	
	output = None
	return output

def getAuthentification():
    '''
    '''
    consumer_key = 'NFrQRU4n6qbYp5yq6VGzeCE2G'
    consumer_secret = '4vq5htdhlRotknGQj3cNAmkLd8AxgNRnrkU3teCQ5JPRPZu6sR'
    access_token = '2504385001-vseO1UJKEFJxJJ7JxKboBC1590089oiJmxYB7W1'
    access_token_secret = '0kxaHK6Kz2s6r3myltsMLFjrDV3OQXLJoox9NzfeXF8uH'

    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    api = tweepy.API(auth)
    
    output = auth, api
	
    return output

def formatValue(element) :
    point = element[0][0]
    group = element[0][1]   
    center = element[0][2]
    idPoint = element[1]
    distance = sqrt(sum([x**2 for x in (point - center)]))
    
    return [group,idPoint, distance]#if (distance < bestDistance[group])
    
class GetStreamListener(tweepy.StreamListener, ssm.Analysis, ssm.WordManagement):
    '''
    '''
    def __init__(self, api, socketio, sc, keyword):
        #self.db = pymongo.MongoClient("localhost", 27017).CheriChampion
        self.api = api
        super(tweepy.StreamListener, self).__init__()
        ssm.Analysis.__init__(self)
        ssm.WordManagement.__init__(self)
        self.ss = socketio
        self.count = -1
        self.sc = sc
        self.nbTopic = 10
        self.nbCluster = 5
        self.graphCount = 0
        self.tweet = []
        self.preparedToken = []
        self.keyword = keyword
        self.minNbTwit = 25
    
    def backThreadLdaKmean(self,corpus, dictionary) :
        '''
        '''
        myCorpus = corpus
        myTopicDocument, myDocumentTopic = self.getDocumentTopic(myCorpus, dictionary)
        nb = len(myCorpus)
        # update edges with correlation
        ssm.getCorrelation(nb, myDocumentTopic, self.ss)
        
        myKmeanModel = KMeans(self.nbCluster, max_iter=10,n_init=10, random_state = 50, tol=1e-4)
        myKmeanFit = myKmeanModel.fit(myDocumentTopic)
        
        myPreparedGroup = []
        for idRow,row in enumerate(myDocumentTopic) :
            myPreparedGroup.append([[row, myKmeanFit.predict(row.reshape(1, -1))[0], myKmeanFit.cluster_centers_[myKmeanFit.predict(row.reshape(1, -1)),:][0]],idRow])
            
        myTwitDistanceToCenter = map(formatValue,myPreparedGroup)
        myTwitClosestToCenter = {}
        for group, idPoint, distance in myTwitDistanceToCenter:
            try:
                prev_idPoint, prev_distance = myTwitClosestToCenter[group]
            except KeyError:
                myTwitClosestToCenter[group] = idPoint, distance
            else:
                if (prev_distance > distance):
                    myTwitClosestToCenter[group] = idPoint, distance
        # 
        for item in myPreparedGroup :
            self.ss.emit('updateImage', {'id':item[1],'image':item[0][1]},
                          namespace='/test')
        
        for key,value in myTwitClosestToCenter.items() :
            self.ss.emit('updateField', {'id':self.tweet[value[0]][0] + 1,'text':self.tweet[value[0]][1],'group':key},
                          namespace='/test')

        
    # @app.route
    def on_status(self, status):
        if ((self.keyword.lower() in status.text.lower()) and (ssm.detect_language(status.text) == 'english')):
            if ((status.text.split(' ')[0] != u'RT') and (status.text.split(' ')[0] != u'RT:')):
                if (self.count < self.minNbTwit):
                    tokenizedTwit = self.getTokenized(status.text)
                    if (len(tokenizedTwit) >2) :
                        self.count += 1
                        self.tweet.append([self.count, status.text])
                        self.preparedToken.append([self.count,tokenizedTwit])
                        for word in tokenizedTwit :
                            self.vocab.append(word)
                        self.ss.emit('addTwit',
                            {'node':{'id': self.count + 1, 
                            'label': status.text,
                            'image': '../static/img/icon2.png', 
                            'shape': 'image'}},
                            namespace='/test')
                elif (self.count%self.minNbTwit) :
                    tokenizedTwit = self.getTokenized(status.text, vocab = self.vocab)
                    if (len(tokenizedTwit) >2) :
                        self.count += 1
                        self.tweet.append([self.count, status.text])
                        self.preparedToken.append([self.count,tokenizedTwit])
                        myDictionary, myCorpus = self.getDocumentWordCount(self.preparedToken) 
                        self.ss.emit('addTwit',
                            {'node':{'id': self.count + 1,
                             'label': status.text,
                             'image': '../static/img/icon2.png', 
                             'shape': 'image'}},
                             namespace='/test')
                        thread1 = None
                        if thread1 is None :
                            thread1 = Thread(target = self.backThreadLdaKmean, args=(myCorpus, myDictionary))
                            thread1.start()
                else :                    
                    tokenizedTwit = self.getTokenized(status.text, vocab = self.vocab)
                    if (len(tokenizedTwit) >2) :
                        self.count += 1
                        self.tweet.append([self.count, status.text])
                        self.preparedToken.append([self.count,tokenizedTwit])
                        self.ss.emit('addTwit',
                            {'node':{'id': self.count + 1, 
                            'label': status.text,
                            'image': '../static/img/icon2.png', 
                            'shape': 'image'}},
                            namespace='/test')
                  
                # data ={}
                # data['text'] = status.text
                # data['created_at'] = status.created_at
                # data['geo'] = status.geo
                # data['source'] = status.source
                
                #self.db.Tweets.insert(data)
        
    def on_error(self, status_code):
        print >> sys.stderr, 'Encountered error with status code:', status_code
        return True # Don't kill the stream

    def on_timeout(self):
        print >> sys.stderr, 'Timeout...'
        return True # Don't kill the stream
        
        
