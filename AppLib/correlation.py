import AppLib as ssm
# spark tools
# from pyspark.mllib.stat import Statistics
from scipy.sparse import csc_matrix
import numpy as np 
from scipy.spatial.distance import cdist

from flask import Flask, render_template, session, request
from flask.ext.socketio import SocketIO, emit, join_room, leave_room, \
    close_room, disconnect

    
def getCorrelation(nb,documentTopic, socketio):
    '''
    '''
    
    # output = csc_matrix(np.corrcoef(documentTopic))

    topicDocument = np.transpose(documentTopic)
    # print(documentTopic)
    output = csc_matrix(cdist(documentTopic, documentTopic, 'mahalanobis', VI=np.linalg.pinv(np.cov(topicDocument))))
    # print(Y)
    # mahalanobis(x,y,np.linalg.inv(np.cov(topicDocument)))

    # print(output)
    # print(np.percentile(output.toarray().flatten(), [5,10,15,20]))
    percentileHigh = np.percentile(output.toarray().flatten(), 10)
    # print(percentileHigh)
    output[output >= percentileHigh] = 0
    
    output.setdiag(0, k=0)
    output.eliminate_zeros()
    
    output = zip(output.nonzero()[0],output.nonzero()[1])
    output=[aa for aa in output if (aa[0]<aa[1])]
    # print(output)
    
    for pair in output :
        socketio.emit('updateEdge', {'node1':pair[0], 'node2':pair[1]},
                      namespace='/test')
    # socketio.emit('updateEdge', {'node1':np.random(0,nb), 'node2':np.random(0,nb)},
                  # namespace='/test')