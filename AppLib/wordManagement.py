from nltk.tokenize import RegexpTokenizer
from stop_words import get_stop_words
from nltk.stem.porter import PorterStemmer
from gensim import corpora, models
import gensim

# from pyspark.mllib.linalg import SparseVector, Vectors
    
# create sample documents
# doc_a = "Brocolli is good to eat. My brother likes to eat good brocolli, but not my mother."
# doc_b = "My mother spends a lot of time driving my brother around to baseball practice."
# doc_c = "Some health experts suggest that driving may cause increased tension and blood pressure."
# doc_d = "I often feel pressure to perform well at school, but my mother never seems to drive my brother to do better."
# doc_e = "Health professionals say that brocolli is good for your health." 

# compile sample documents into a list
# doc_set = [doc_a, doc_b, doc_c, doc_d, doc_e]

# list for tokenized documents in loop

class WordManagement():
    '''
    '''
    
    def __init__(self):
        self.vocab = []
    
    def getTokenized(self, doc, vocab = []) :
        '''
        '''
        tokenizer = RegexpTokenizer(r'\w+')

        # create English stop words list
        en_stop = get_stop_words('en')

        # Create p_stemmer of class PorterStemmer
        p_stemmer = PorterStemmer()
        
        # loop through document list
        #for i in docSet:
            
        # clean and tokenize document string
        raw = doc.lower()
        tokens = tokenizer.tokenize(raw)

        # remove stop words from tokens
        stopped_tokens = [i for i in tokens if not i in en_stop]
        
        # stem tokens
        stemmed_tokens = [p_stemmer.stem(i) for i in stopped_tokens]
        
        stemmed_tokens = [token for token in stemmed_tokens if len(token) != 10]
        stemmed_tokens = [token for token in stemmed_tokens if token not in ["http",self.keyword]]
        ## only if word is in vocab: when vocab is finished
        if (len(vocab) > 1) :
            stemmed_tokens = [token for token in stemmed_tokens if token in vocab]
        stemmed_tokens = [token for token in stemmed_tokens if len(token) > 2]
        
        # add tokens to list
        return stemmed_tokens 

            
    def getDocumentWordCount(self, allText) :
            # turn our tokenized documents into a id <-> term dictionary
            dictionary = corpora.Dictionary([text[1] for text in allText])
            myLength = max(dictionary.keys())
            # dictionary.filter_extremes()
            
            # convert tokenized documents into a document-term matrix
            # corpus = [(int(text[0]),(myLength, {(x[0]-1):float(x[1]) for x in dictionary.doc2bow(text[1])})) for text in allText]
            corpus = [dictionary.doc2bow(text[1]) for text in allText]
            
            return dictionary, corpus
        
# generate LDA model
# ldamodel = gensim.models.ldamodel.LdaModel(corpus, num_topics=2, id2word = dictionary, passes=20)



# I was not able to try this section since unable to import textmining
# import textmining
# Create some very short sample documents
# Initialize class to create term-document matrix
# tdm = textmining.TermDocumentMatrix()
# Add the documents
# for i in texts :
    # tdm.add(i)
# print(tdm)        
        