import numpy as np
from math import sqrt

def formatValue(element) :
	point = element[0][0]
	group = element[0][1]   
	center = element[0][2]
	idPoint = element[1]
	distance = sqrt(sum([x**2 for x in (point - center)]))
	return (group,[idPoint, distance])#if (distance < bestDistance[group])

def getMaxSpecial(x,y) :
    if x[1] < y[1] :
        output = x
    else :
        output = y
	return output  
