##### spark installation


###################################################################
#######               StreamingSparkMarketing              ########
###################################################################

###################################################################
## Module 1: catchData

API Twitter
input url de l'api, paramaters for twit selection
ouptut stream of twits

storage ?

###################################################################
## Module 9: loadData

path from mongodb and load into RDD

###################################################################
## Module 2: prepareData

get features

###################################################################
## Module 3: lda

topic model
input: RRD document x [index, DenseVector(word count)]]  (could be sparse ?)
output: RDD document x topic (proba)

###################################################################
## Module 10: getCorrelation

input: RDD document x topic (proba)
ouput: document x document (correlatio)

###################################################################
## Module 4: prepareOutput

###################################################################
## Module 5: visualizationOutput

###################################################################
## Module 6: stream/start/stop

###################################################################
## Module 7: first spark instance

###################################################################
## Module 8: spark streaming

#####################################################################
## pseudo code

Choose a # in the list:
(param : tag to follow, timer, nb launch centrality detection nb topic lda, nb center k means)
    initiate RDD for featured twits
    Begin to listen:
        initiate a RDD for brut twits
        initiate X sec timer
        authentification
        catch error
        on status
            store info in mongodb
            add twit text to RDD
            if timer = 0:
                reinitialize stream part:
                    timer = X
                    copy RDD for brut twits
                    reboot RDD for brut twits
                process data:
                    add features to RRD for featured twits
                    dump featured twits to mongodb
                    if #RDD == 1:
                        just display (update json)
                    elif #RDD < minTwitLimit:
                        run LDA
                        get correlation
                        display (up json)
                    elif: #RDD >= minTwitLimit:
                        run LDA
                        get correlation
                        run Kmean
                        colect 3 central twits per cluster
                        display (up json)



#####################################################################
## TODO
# display list of hot twits topics, hashtag ? 
# list of past events in demo non-streaming mode, with accelerating time ?
